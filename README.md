# General information
Wega Informatik AG has provided two own in-house made challenges. A group can choose one or both challenges, as they are both very much connected to each other. 

Below you can find the summaries of the challenges again. Furthermore, the presentation with more in detail explanation can be found in this project.

Remember, the focus is rather on getting an idea on the digitlaization of business processes and automated data handling and processing, than having the perfect outcome with respect to machine learning and science. Ideally, you will generate a fully digitalized business process for clinical data handling and and end-to-end workflow for data transformation and output generation, with integrated decision modelling.

## Summary challenge 1
From pseudonymized clinical data to targeted noise.

Introduce dedicated noise into clinical trail data, to achieve an intermediate state between anonymized and pseudonymized data. With that, data do not fall under GDPR regulation, but will maintain the right amount of granularity information for scientific accurate outcomes.

Potential Outputs:

✓ Draw business and data flows with continously autmated data integration in mind

✓ Introduce different noise catagories, develop machine learning models and benchmark them

✓ Develop pipeline evaluation metric and process related KPI’s


## Summary challenge 2
There is never enough data – create synthetic clinical data.

Goal: Biomedical and clinical data are expensive to generate. Recently, generative models were introduced to generate synthetic data for e.g.: machine learning training purposes. Therefore, apply generative models to produce more data.

Potential Outputs:

✓ Draw business and data flows with continuously automated data integration, generation, and evaluation in mind

✓ Apply scientific-domain specific generative models for synthetic data generation in the field of clinical data, and benchmark the different models for their applicability

✓ Develop evaluation metric and visualization tools to compare real and synthetic data, and process related KPI’s


# How to start and procedure

After you have been choosing your challenge, you can create in the respective group your own project repository and start working. Use your README like final document you would hand in to your manager after the work has been done. 

## Data

Data are provided in this project repository in the folder called: "Clinical Study Data".

## Recommended Method, Tools and Reads

1. Always start with creating user-stories, it helps you to understand and define what you want to achieve, after that map the requirements. If wished, one of the wega people can act as your "end-suer" who you would have to interview.
2. Visualize the business process with a drawing (e.g.: in Camunda or Draw-io). If you want to go the whole way with a fully automated business process digitalization, then Camunda is the tool to go.
3. Draw the data flows 
4. Explore your data and start working (R or Python)
5. Think on the fly which steps can be automated and think also where manual control check points might be good to implement
6. Implement from the beginning CI/CD practices when developing, testing and deploying your machine learning models (do this directly in GitLab and Azure if possible and familiar with)
7. Organize your steps in an end-to-end workflow.


Following reads are recommended to start with; feel free to ask questions and reasearch more on the topic(s)

https://www.heavy.ai/technical-glossary/data-anonymization

https://mostly.ai/blog/3-reasons-to-drop-classic-anonymization-and-upgrade-to-synthetic-data

https://medium.com/@brperry/pseudonymization-anonymization-gdpr-3dc8405dd465

https://medium.com/@mentisinc/anonymization-vs-pseudonymization-faca97676eb2

https://medium.com/@mentisinc/anonymization-vs-pseudonymization-faca97676eb2


Generally, the concept of introducing noise to clinical data has been further developed as business model to so-called "data clean rooms", and is levraged by a start-up company called "Decentriq" --> https://blog.decentriq.com/differential-privacy-as-a-way-to-protect-first-party-data/

#